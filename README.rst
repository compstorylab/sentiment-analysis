
##################################################################################################
SentimentLexicographer: Augmenting semantic lexicons using word embeddings and transfer learning
##################################################################################################

.. contents::


Description
###########


Sentiment-aware intelligent systems are essential to an enormous set of applications
from
marketing and political campaigns
to recommender systems
to behavioral and social psychology
to intelligence and national security.
These sentiment-aware intelligent systems are driven by language models,
which fall into two paradigms: lexicon-based or contextual.

Although recent contextual models are relevant to a wide range of applications,
we still see a tremendous and constant demand for lexicon-based models
because of their interpretability, simplicity and ease of use.
In particular,
researchers are often interested in both the linguistic and sociotechnical details of a given
sentiment shift---which words contribute most to either a positive or negative sentiment trend.

Lexicon dictionaries,
however,
need to be updated periodically
to support new words and expressions that were not examined when the dictionaries were outsourced.
Crowdsourcing annotations for semantic dictionaries is an arduous task.

Here,
we propose two models for predicting sentiment scores to augment semantic lexicons
at a relatively low cost using word embeddings and transfer learning.



Models
########


Input embeddings
----------------

.. image:: data/embeddings.png
    :alt: Embeddings


Token model
------------
Our first model is a simple and shallow neural network
initialized with pre-trained
`FastText <https://fasttext.cc/docs/en/crawl-vectors.html>`_  word embeddings.
The model uses fixed word representations to gauge the sentiment score for a given expression.
While still being able to learn a non-linear mapping between the words and their sentiment scores,
the model only considers the individual words
as input---enriching its internal utility function with subword representations
(see `fasttext_models.py <src/fasttext_models.py>`__ for technical details).


Dictionary model
-----------------
Bridging the link between lexicon-based and contextualized models,
we also propose a deep Transformer-based model that uses word definitions
to estimate their sentiment scores.
The contextualized nature of the input data allows our model
to accurately estimate the expressed sentiment score for a given word based on its lexical meaning.
(see `transformer_models.py <src/transformer_models.py>`__ for technical details).

Usage
########

To get started, please make sure to install
`Tensorflow-GPU <https://www.tensorflow.org/install/gpu>`__
and
`Transformers <https://huggingface.co/transformers/installation.html>`__.

You can use the `predict.py <src/predict.py>`__ script to get predictions for a given word
(see also `predict_table.py <src/predict_table.py>`__ to get predictions for a `TSV` file).


.. code:: shell

    usage: predict.py [-h] {labmt,ousiometer,vad} ... ensemble ngram

    positional arguments:
      {labmt,ousiometer,vad}
                            Arguments for specific action.
      ensemble              path to a pretrained model(s)
      ngram                 ngram to score

    optional arguments:
      -h, --help            show this help message and exit


Below we show an example code for getting a prediction for the word "coronavirus" using the Token model:

``python predict.py labmt ../models/fasttext "coronavirus"``

.. code:: shell

    Predictions: 100%|██████████████████████████████████████████████████████████████████████| 100/100 [00:00<00:00, 347.54it/s]
    Predictions: 100%|██████████████████████████████████████████████████████████████████████| 100/100 [00:00<00:00, 630.15it/s]
    Predictions: 100%|██████████████████████████████████████████████████████████████████████| 100/100 [00:00<00:00, 588.96it/s]
    Predictions: 100%|██████████████████████████████████████████████████████████████████████| 100/100 [00:00<00:00, 597.95it/s]
    Predictions: 100%|██████████████████████████████████████████████████████████████████████| 100/100 [00:00<00:00, 595.98it/s]

    Input: `coronavirus`

    Token Embeddings:
    ['virus', 'avir', 'vir', 'nav', 'coronavirus', 'ona', 'ron', 'rus', 'onav', 'rona', 'viru',
     'ronav', 'irus', 'avi', 'orona', 'cor', 'oro', 'coron', 'onavi', 'oron', 'coro', 'iru',
     'aviru', 'navi', 'navir']

    ----------------------------------------------------------------------------------------------------

    Predicted: [1.75406178] stdev: [0.39573207]
    Ground-truth: 1.34



And using the Dictionary model:

``python predict.py labmt ../models/transformer "happy"``

.. code:: shell

    Predictions: 100%|██████████████████████████████████████████████████████████████████████| 100/100 [00:00<00:00, 101.90it/s]
    Predictions: 100%|██████████████████████████████████████████████████████████████████████| 100/100 [00:00<00:00, 114.94it/s]
    Predictions: 100%|██████████████████████████████████████████████████████████████████████| 100/100 [00:00<00:00, 115.86it/s]
    Predictions: 100%|██████████████████████████████████████████████████████████████████████| 100/100 [00:00<00:00, 113.80it/s]
    Predictions: 100%|██████████████████████████████████████████████████████████████████████| 100/100 [00:00<00:00, 110.35it/s]

    Input: `happy`

    Definition:
    happy: happy: feeling or showing pleasure or contentment. fortunate and convenient. inclined to use a specified thing excessively or at random.


    Dictionary Embeddings:
    ['happy' ':' 'happy' ':' 'feeling' 'or' 'showing' 'pleasure' 'or'
     'content' '##ment' '.' 'fortunate' 'and' 'convenient' '.' 'inclined' 'to'
     'use' 'a' 'specified' 'thing' 'excessive' '##ly' 'or' 'at' 'random' '.']
    ----------------------------------------------------------------------------------------------------

    Predicted: [7.89987358] stdev: [0.37425384]
    Ground-truth: 8.3



Citation
########

For more information please see the following paper:

    Alshaabi, T., Van Oort, C., Fudolig, M., Arnold, M. V.,
    Danforth, C. M., & Dodds, P. S.
    `Augmenting semantic lexicons using word embeddings and transfer learning
    <https://arxiv.org/abs/2109.09010>`__.
    *arXiv preprint* (2021).
