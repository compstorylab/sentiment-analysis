
import argparse
from argparse import ArgumentDefaultsHelpFormatter
from operator import attrgetter
from pathlib import Path
import tensorflow as tf


class SortedMenu(ArgumentDefaultsHelpFormatter):
    def add_arguments(self, actions):
        actions = sorted(actions, key=attrgetter("option_strings"))
        super(SortedMenu, self).add_arguments(actions)


def get_subparsers():

    physical_devices = tf.config.list_physical_devices('GPU')
    for gpu_instance in physical_devices:
        tf.config.experimental.set_memory_growth(gpu_instance, True)

    parser = argparse.ArgumentParser(
        formatter_class=SortedMenu,
        description="Copyright (c) 2021 The Computational Story Lab. "
                    "Licensed under the MIT License",
    )

    subparsers = parser.add_subparsers(
        help="Arguments for specific action.", dest="dtype"
    )
    subparsers.required = True

    labmt_parser = subparsers.add_parser("labmt")
    labmt_parser.add_argument(
        "--scores",
        type=Path,
        default="../data/labmt.tsv",
        help="path to labMT scores",
    )
    labmt_parser.add_argument(
        "--tokenizer",
        type=Path,
        default="../data/labmt_tokenizer.json",
        help="path to trained tokenizer json vocab file",
    )

    ousiometer_parser = subparsers.add_parser("ousiometer")
    ousiometer_parser.add_argument(
        "target",
        type=str,
        help="name of the target feature",
    )
    ousiometer_parser.add_argument(
        "--scores",
        type=Path,
        default="../data/ousiometer.tsv",
        help="path to Ousiometer scores",
    )
    ousiometer_parser.add_argument(
        "--tokenizer",
        type=Path,
        default="../data/ousiometer_tokenizer.json",
        help="path to trained tokenizer json vocab file",
    )

    vad_parser = subparsers.add_parser("vad")
    vad_parser.add_argument(
        "target",
        type=str,
        help="name of the target feature",
    )
    vad_parser.add_argument(
        "--scores",
        type=Path,
        default="../data/vad.tsv",
        help="path to VAD scores",
    )
    vad_parser.add_argument(
        "--tokenizer",
        type=Path,
        default="../data/vad_tokenizer.json",
        help="path to trained tokenizer json vocab file",
    )

    return parser
