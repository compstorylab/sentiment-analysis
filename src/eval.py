import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


import time
import sys
import logging
from pathlib import Path

from cli import get_subparsers

from utils import eval_model
from utils import load_labmt_scores
from utils import load_ousiometer_scores
from utils import load_vad_scores

from vis import plot_mae_heatmap
from vis import plot_labmt_mae_dists
from vis import plot_ousiometer_mae_dists
from vis import plot_conf
from vis import plot_errorbars
from vis import plot_conf_effectiveness


def parse_args(args):
    parser = get_subparsers()

    parser.add_argument(
        "ensemble",
        type=Path,
        help="path to a pretrained model(s)",
    )

    parser.add_argument(
        "--batch_size",
        type=int,
        default=512,
        help="batch size to use for training",
    )

    return parser.parse_args(args)


logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s'
)


def main(args=None):
    timeit = time.time()
    args = parse_args(args)

    out = Path(args.ensemble/'plots')
    out.mkdir(parents=True, exist_ok=True)

    if args.dtype == "labmt":
        scores = eval_model(
            args.ensemble,
            scores=load_labmt_scores(args.scores),
            target='happiness',
            batch_size=args.batch_size,
            tokenizer=args.tokenizer if 'fasttext' in str(args.ensemble) else None,
        )
        plot_labmt_mae_dists(scores, savedir=out)
        plot_mae_heatmap(scores, dim='happiness', savedir=out)
        plot_conf(scores, dim='happiness', savedir=out)
        plot_errorbars(scores, dim='happiness', savedir=out)
        plot_conf_effectiveness(scores, dim='happiness', minn=1, maxx=9, step=2, savedir=out)

    elif args.dtype == "ousiometer":
        scores = eval_model(
            args.ensemble,
            scores=load_ousiometer_scores(args.scores),
            target=args.target,
            batch_size=args.batch_size,
            tokenizer=args.tokenizer if 'fasttext' in str(args.ensemble) else None,
        )
        plot_ousiometer_mae_dists(scores, dim=args.target, savedir=out)
        plot_mae_heatmap(scores, dim=args.target, savedir=out)
        plot_conf(scores, dim=args.target, savedir=out)
        plot_conf_effectiveness(scores, dim=args.target, minn=-.66, maxx=.66, step=.3, savedir=out)

    elif args.dtype == "vad":
        scores = eval_model(
            args.ensemble,
            scores=load_vad_scores(args.scores),
            target=args.target,
            batch_size=args.batch_size,
            tokenizer=args.tokenizer if 'fasttext' in str(args.ensemble) else None,
        )
        plot_ousiometer_mae_dists(scores, dim=args.target, savedir=out)
        plot_mae_heatmap(scores, dim=args.target, savedir=out)
        plot_conf(scores, dim=args.target, savedir=out)
        plot_conf_effectiveness(scores, dim=args.target, minn=0, maxx=1, step=.25, savedir=out)

    else:
        logging.error("Unknown LEX type!")
        return

    logging.info(f"Total time elapsed: {time.time() - timeit:.2f} sec.")


if __name__ == "__main__":
    main()
