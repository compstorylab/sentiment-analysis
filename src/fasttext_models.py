import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


import sys
import logging

import numpy as np
from tensorflow.keras import Model
from tensorflow.keras import layers

logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s'
)
logger = logging.getLogger(__name__)


class Default(Model):
    """
    Args:
        embedding: pretrained word embedding matrix
        max_token_len: max number of subwords allowed for input

    Returns:
        Keras Model
    """

    def __init__(self, embedding: np.array, max_token_len: int):
        super(Default, self).__init__()
        self.max_token_len = max_token_len
        self.embedding = layers.Embedding(
            input_dim=embedding.shape[0],
            output_dim=embedding.shape[1],
            weights=[embedding],
            input_length=max_token_len,
        )
        self.dropout = layers.Dropout(0.5)
        self.pool = layers.GlobalAvgPool1D()
        self.regressor = layers.Dense(1, activation='linear')

    def model(self):
        x = layers.Input(shape=(self.max_token_len,))
        return Model(inputs=x, outputs=self.call(x))

    def circuit(self, inputs, nodes, training):
        c1 = layers.Dense(nodes, activation='relu')(inputs)
        c1 = self.dropout(c1, training=training)
        c1 = layers.Dense(nodes, activation='relu')(c1)
        c1 = self.dropout(c1, training=training)
        c1 = layers.Dense(nodes, activation='relu')(c1)
        c1 = self.dropout(c1, training=training)

        c2 = layers.Dense(nodes, activation='relu')(inputs)
        c2 = self.dropout(c2, training=training)
        c2 = layers.Dense(nodes, activation='relu')(c2)
        c2 = self.dropout(c2, training=training)
        c2 = layers.Dense(nodes, activation='relu')(c2)
        c2 = self.dropout(c2, training=training)

        circuits = layers.concatenate([c1, c2], axis=1)
        circuits = layers.Flatten()(circuits)
        return circuits


class Baseline(Default):
    def call(self, inputs, training=True, **kwargs):

        x = self.embedding(inputs)
        x = self.pool(x)

        x = layers.Dense(128, activation='relu')(x)
        x = self.dropout(x, training=training)

        x = layers.Dense(64, activation='relu')(x)
        x = self.dropout(x, training=training)

        x = layers.Dense(32, activation='relu')(x)
        return self.regressor(x)


class ParallelCircuits(Default):
    def call(self, inputs, training=True, **kwargs):
        emb = self.embedding(inputs)

        c1 = layers.Dense(128, activation='relu')(emb)
        c1 = layers.Dense(64, activation='relu')(c1)
        c1 = self.dropout(c1, training=training)

        c2 = layers.Dense(128, activation='relu')(emb)
        c2 = layers.Dense(64, activation='relu')(c2)
        c2 = self.dropout(c2, training=training)

        c3 = layers.Dense(128, activation='relu')(emb)
        c3 = layers.Dense(64, activation='relu')(c3)
        c3 = self.dropout(c3, training=training)

        circuits = layers.concatenate([c1, c2, c3], axis=1)
        circuits = layers.Flatten()(circuits)
        x = layers.Dense(32, activation='relu')(circuits)
        return self.regressor(x)


class DeepParallelCircuits(Default):
    def call(self, inputs, training=True, **kwargs):
        emb = self.embedding(inputs)

        circuits = self.circuit(emb, nodes=256, training=training)
        for i in range(10):
            circuits = self.circuit(circuits, nodes=256, training=training)

        x = layers.Dense(64, activation='relu')(circuits)
        x = self.dropout(x, training=training)
        return self.regressor(x)
