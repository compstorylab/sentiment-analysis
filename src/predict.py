import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from pathlib import Path
from cli import get_subparsers
from utils import load_labmt_scores
from utils import load_vad_scores
from utils import load_ousiometer_scores
from utils import ensemble_predict
from utils import get_def
from utils import tokenize
from utils import char_ngram_tokenize


def parse_args(args):
    parser = get_subparsers()

    parser.add_argument(
        "ensemble",
        type=Path,
        help="path to a pretrained model(s)",
    )

    parser.add_argument(
        "ngram",
        type=str,
        help="ngram to score",
    )

    return parser.parse_args(args)


def main(args=None):
    args = parse_args(args)

    if args.dtype == "labmt":
        dataset = load_labmt_scores(args.scores)
        scores = dataset['happiness']

    elif args.dtype == 'ousiometer':
        dataset = load_ousiometer_scores(args.scores)
        scores = dataset[args.target]

    elif args.dtype == 'vad':
        dataset = load_vad_scores(args.scores)
        scores = dataset[args.target]

    else:
        raise Exception("Unknown LEX type!")

    definition = f"{args.ngram}: {get_def(args.ngram)}"
    _, pred, stdev = ensemble_predict(
        args.ensemble,
        tokens=[args.ngram] if 'fasttext' in str(args.ensemble) else [definition],
        min_val=scores.min(),
        max_val=scores.max(),
        batch_size=100,
        tokenizer_path=args.tokenizer if 'fasttext' in str(args.ensemble) else None,
    )

    token_emb = char_ngram_tokenize([args.ngram], args.tokenizer)['subwords'][0]
    def_emb = tokenize([definition])['subwords'][0]

    if 'fasttext' in str(args.ensemble):
        print(f"\n\nInput: `{args.ngram}`")
        print(f"\nToken Embeddings:\n{token_emb}\n")
        print('-' * 100)
        print(f"\nPredicted: {pred}\tstdev: {stdev}")
        print(f"Ground-truth: {scores.to_dict().get(args.ngram.lower())}")

    else:
        print(f"\n\nInput: `{args.ngram}`")
        print(f"\nDefinition:\n{definition}\n")
        print(f"\nDictionary Embeddings:\n{def_emb}")
        print('-'*100)
        print(f"\nPredicted: {pred}\tstdev: {stdev}")
        print(f"Ground-truth: {scores.to_dict().get(args.ngram.lower())}")


if __name__ == "__main__":
    main()
