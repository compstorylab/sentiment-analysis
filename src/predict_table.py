import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from pathlib import Path
import argparse
from utils import multiprocess
from utils import get_def
from utils import load_unknown_table
from utils import ensemble_predict


def parse_args(args):
    parser = argparse.ArgumentParser(
        description="Copyright (c) 2021 The Computational Story Lab. "
                    "Licensed under the MIT License",
    )

    parser.add_argument(
        "ensemble",
        type=Path,
        help="path to a pretrained model(s)",
    )

    parser.add_argument(
        "table",
        type=Path,
        help="path to a tsv/csv file",
    )

    parser.add_argument(
        "--tokenizer",
        type=Path,
        default="../data/labmt_tokenizer.json",
        help="path to trained tokenizer json vocab file",
    )

    return parser.parse_args(args)


def main(args=None):
    args = parse_args(args)

    table = load_unknown_table(args.table)

    if 'transformer' in str(args.ensemble):
        table['def'] = multiprocess(
            get_def,
            table.index,
            cores=-1
        )

    table['subwords'], table['prediction'], table['stdev'] = ensemble_predict(
        args.ensemble,
        tokens=table.index.tolist() if 'fasttext' in str(args.ensemble) else table['def'].tolist(),
        min_val=1,
        max_val=9,
        batch_size=512,
        tokenizer_path=args.tokenizer if 'fasttext' in str(args.ensemble) else None,
    )

    if 'fasttext' in str(args.ensemble):
        table.to_csv(
            args.table.with_name(f'{args.table.stem}_token_model_predictions.tsv'),
            sep='\t'
        )
    else:
        table.to_csv(
            args.table.with_name(f'{args.table.stem}_dictionary_model_predictions.tsv'),
            sep='\t'
        )


if __name__ == "__main__":
    main()
