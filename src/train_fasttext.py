
import time
import sys
import logging
from pathlib import Path
from datetime import datetime
import pandas as pd
import numpy as np
from tensorflow.keras.callbacks import TensorBoard
from tensorflow.keras.callbacks import CSVLogger
from tensorflow.keras.callbacks import ModelCheckpoint
from sklearn.model_selection import KFold

from cli import get_subparsers
from utils import preprocess
from utils import load_labmt_scores
from utils import load_vad_scores
from utils import load_ousiometer_scores

from vis import plot_cross_val
from utils import eval_model

import fasttext_models


def parse_args(args):
    parser = get_subparsers()

    parser.add_argument(
        "--model",
        type=str,
        default="baseline",
        help="model codename",
    )

    parser.add_argument(
        "--embedding",
        type=Path,
        default="../data/cc.en.300.vec",
        help="path to FastText pretrained embedding",
    )

    parser.add_argument(
        "--batch_size",
        type=int,
        default=128,
        help="batch size to use for training",
    )

    parser.add_argument(
        "--epochs",
        type=int,
        default=200,
        help="number of training iterations",
    )

    parser.add_argument(
        "--folds",
        type=int,
        default=5,
        help="number of folds for cross-validation",
    )

    parser.add_argument(
        "--max_token_len",
        type=int,
        default=50,
        help="max number of subwords allowed to be scored as a token",
    )

    parser.add_argument(
        "--output",
        type=Path,
        default="../models",
        help="path to save models",
    )

    return parser.parse_args(args)


logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s'
)


def main(args=None):
    timeit = time.time()
    args = parse_args(args)
    outdir = Path(f"{args.output}/{args.dtype}/fasttext/{args.model}")
    outdir.mkdir(exist_ok=True, parents=True)

    logbook = f"{outdir}/{args.dtype}_{args.model}_logbook.csv"
    logs = Path(f"../logs/{args.dtype}/{args.model}/{datetime.now().strftime('%Y-%m-%d-%H-%M')}")
    logs.mkdir(exist_ok=True, parents=True)

    if args.dtype == "labmt":
        dataset = load_labmt_scores(args.scores)
        targets = dataset['happiness'].values

    elif args.dtype == 'ousiometer':
        dataset = load_ousiometer_scores(args.scores)
        targets = dataset[args.target].values

    elif args.dtype == 'vad':
        dataset = load_vad_scores(args.scores)
        targets = dataset[args.target].values
    else:
        logging.error("Unknown LEX type!")
        return

    if args.model == "baseline":
        regressor = fasttext_models.Baseline
    elif args.model == "circuits":
        regressor = fasttext_models.ParallelCircuits
    elif args.model == "deep":
        regressor = fasttext_models.DeepParallelCircuits
    else:
        logging.error("Unknown model!")
        return

    cv = CSVLogger(
        logbook,
        append=True,
    )

    tb = TensorBoard(
        log_dir=logs,
        profile_batch='500,520',
    )

    log_records = []
    for t in range(10):

        train_df = dataset.sample(frac=.8)
        test_df = dataset.drop(train_df.index)

        X_train, embedding = preprocess(
            train_df,
            embedding=args.embedding,
            max_token_len=args.max_token_len,
            embedding_space=300,
            subword_parser=args.tokenizer,
        )
        y_train = train_df['happiness'].values

        kf = KFold(n_splits=args.folds)
        for i, (train, val) in enumerate(kf.split(X_train, y_train)):
            cp = ModelCheckpoint(
                filepath=f"{outdir}/model_{i+1}",
                monitor="val_loss",
                save_best_only=True,
                verbose=1,
            )

            m = regressor(
                embedding=embedding,
                max_token_len=args.max_token_len,
            ).model()

            logging.info(m.summary())

            m.compile(
                loss='mse',
                optimizer='adam',
                metrics=['mae', 'mse']
            )

            m.fit(
                X_train[train],
                y_train[train],
                batch_size=args.batch_size,
                epochs=args.epochs,
                callbacks=[cv, tb, cp],
                validation_data=(X_train[val], y_train[val]),
                shuffle=True
            )

        # logbook = pd.read_csv(logbook, header=0, index_col=0)
        # logbook['fold'] = np.repeat(range(1, args.folds+1), logbook.shape[0]/args.folds)
        # plot_cross_val(logbook, savepath=f'{outdir}/{args.dtype}_{args.model}_crossval')

        scores = eval_model(
            outdir,
            scores=test_df,
            target='happiness',
            batch_size=args.batch_size,
            tokenizer=args.tokenizer if 'fasttext' in str(outdir) else None,
        )

        res = scores['mae'].round(2).describe(percentiles=[.25, .5, .75, .85, .95])
        res.to_csv(f'{outdir}/{args.dtype}_{args.model}_test_{t}.csv')
        log_records.append(res)
        logging.info(res)

    df = pd.concat(log_records).groupby(level=0).mean()
    logging.info(df)
    df.to_csv(f'{outdir}/{args.dtype}_{args.model}_test.csv')

    logging.info(f"Total time elapsed: {time.time() - timeit:.2f} sec.")


if __name__ == "__main__":
    main()

