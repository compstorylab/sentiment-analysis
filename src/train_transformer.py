import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import time
import sys
import logging
from pathlib import Path
from datetime import datetime
import pandas as pd
import numpy as np

from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import TensorBoard
from tensorflow.keras.callbacks import CSVLogger
from tensorflow.keras.callbacks import ModelCheckpoint
from sklearn.model_selection import KFold

from cli import get_subparsers
from utils import load_labmt_scores
from utils import load_vad_scores
from utils import load_ousiometer_scores
from utils import tokenize

from vis import plot_cross_val
from utils import eval_model

import transformer_models


def parse_args(args):
    parser = get_subparsers()

    parser.add_argument(
        "--model",
        type=str,
        default="baseline",
        help="model codename",
    )

    parser.add_argument(
        "--embedding",
        type=Path,
        default="../data/cc.en.300.vec",
        help="path to FastText pretrained embedding",
    )

    parser.add_argument(
        "--batch_size",
        type=int,
        default=128,
        help="batch size to use for training",
    )

    parser.add_argument(
        "--epochs",
        type=int,
        default=500,
        help="number of training iterations",
    )

    parser.add_argument(
        "--folds",
        type=int,
        default=5,
        help="number of folds for cross-validation",
    )

    parser.add_argument(
        "--max_token_len",
        type=int,
        default=50,
        help="max number of subwords allowed to be scored as a token",
    )

    parser.add_argument(
        "--output",
        type=Path,
        default="../models",
        help="path to save models",
    )

    return parser.parse_args(args)


logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s'
)


def main(args=None):
    timeit = time.time()
    args = parse_args(args)
    outdir = Path(f"{args.output}/{args.dtype}/transformer/{args.model}")
    outdir.mkdir(exist_ok=True, parents=True)

    logbook = f"{outdir}/{args.dtype}_transformer_logbook.csv"
    logs = Path(f"../logs/{args.dtype}/transformer/{datetime.now().strftime('%Y-%m-%d-%H-%M')}")
    logs.mkdir(exist_ok=True, parents=True)

    cv = CSVLogger(
        logbook,
        append=True,
    )

    tb = TensorBoard(
        log_dir=logs,
        profile_batch='500,520',
    )

    if args.dtype == "labmt":
        dataset = load_labmt_scores(args.scores)
        targets = dataset['happiness'].values

    elif args.dtype == 'ousiometer':
        dataset = load_ousiometer_scores(args.scores)
        targets = dataset[args.target].values

    elif args.dtype == 'vad':
        dataset = load_vad_scores(args.scores)
        targets = dataset[args.target].values
    else:
        logging.error("Unknown LEX type!")
        return

    if args.model == "baseline":
        regressor = transformer_models.Baseline
    elif args.model == "lstm":
        regressor = transformer_models.LSTM
    else:
        logging.error("Unknown model!")
        return

    log_records = []
    for t in range(10):

        train_df = dataset.sample(frac=.8)
        test_df = dataset.drop(train_df.index)

        input_seq = train_df['def'].fillna('').values
        targets = train_df['happiness'].values

        kf = KFold(n_splits=args.folds)
        for i, (train, val) in enumerate(kf.split(input_seq, targets)):

            X_train = tokenize(
                docs=input_seq[train],
                max_length=args.max_token_len,
                verbose=1
            )

            X_val = tokenize(
                docs=input_seq[val],
                max_length=args.max_token_len,
                verbose=1
            )

            cp = ModelCheckpoint(
                filepath=f"{outdir}/model_{i+1}",
                monitor="val_loss",
                save_best_only=True,
                verbose=1,
                save_format='tf'
            )

            m = regressor(max_length=args.max_token_len).model()
            logging.info(m.summary())

            m.compile(
                loss='mse',
                optimizer=Adam(lr=5e-5),
                metrics=['mae', 'mse']
            )

            m.fit(
                X_train['input_ids'],
                targets[train],
                batch_size=args.batch_size,
                epochs=args.epochs,
                callbacks=[cv, tb, cp],
                validation_data=(X_val['input_ids'], targets[val]),
                shuffle=True
            )

        # logbook = pd.read_csv(logbook, header=0, index_col=0)
        # logbook['fold'] = np.repeat(range(1, args.folds+1), logbook.shape[0]/args.folds)
        # plot_cross_val(logbook, savepath=f'{outdir}/{args.dtype}_{args.model}_crossval')

        scores = eval_model(
            outdir,
            scores=test_df,
            target='happiness',
            batch_size=args.batch_size,
            tokenizer=args.tokenizer if 'fasttext' in str(outdir) else None,
        )

        res = scores['mae'].round(2).describe(percentiles=[.25, .5, .75, .85, .95])
        res.to_csv(f'{outdir}/{args.dtype}_{args.model}_test_{t}.csv')
        log_records.append(res)
        logging.info(res)

    df = pd.concat(log_records).groupby(level=0).mean()
    logging.info(df)
    df.to_csv(f'{outdir}/{args.dtype}_{args.model}_test.csv')

    logging.info(f"Total time elapsed: {time.time() - timeit:.2f} sec.")


if __name__ == "__main__":
    main()
