
import time
import sys
import logging
from pathlib import Path
import pandas as pd

import gensim.downloader
from sklearn.svm import SVR
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import Lasso
from sklearn.linear_model import Ridge
from sklearn.linear_model import ElasticNet
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_validate
from sklearn.decomposition import PCA

from cli import get_subparsers
from utils import load_labmt_scores
from utils import load_vad_scores
from utils import load_ousiometer_scores
from utils import load_pretrained_embedding


def parse_args(args):
    parser = get_subparsers()

    parser.add_argument(
        "--model",
        type=str,
        default="svr",
        help="model codename",
    )

    parser.add_argument(
        "--embedding",
        type=str,
        default=None,
        help="codename for pre-trained embedding to use from gensim "
             "(https://github.com/RaRe-Technologies/gensim-data)",
    )

    parser.add_argument(
        "--folds",
        type=int,
        default=5,
        help="number of folds for cross-validation",
    )

    parser.add_argument(
        "--output",
        type=Path,
        default="../models",
        help="path to save models",
    )

    return parser.parse_args(args)


logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s'
)


def main(args=None):
    timeit = time.time()
    args = parse_args(args)
    outdir = Path(f"{args.output}/{args.dtype}/word2vec/{args.model}")
    outdir.mkdir(exist_ok=True, parents=True)

    if args.dtype == "labmt":
        dataset = load_labmt_scores(args.scores)
        targets = dataset['happiness'].values

    elif args.dtype == 'ousiometer':
        dataset = load_ousiometer_scores(args.scores)
        targets = dataset[args.target].values

    elif args.dtype == 'vad':
        dataset = load_vad_scores(args.scores)
        targets = dataset[args.target].values

    else:
        logging.error("Unknown LEX type!")
        return

    if args.model == "svr":
        regressor = SVR(kernel='rbf', max_iter=1000)

    elif args.model == "rf":
        regressor = RandomForestRegressor(n_estimators=300)

    elif args.model == "lasso":
        regressor = Lasso()

    elif args.model == "ridge":
        regressor = Ridge()

    elif args.model == "elastic":
        regressor = ElasticNet()

    else:
        logging.error("Unknown model!")
        return

    if args.embedding is None:
        args.embedding = [
            'fasttext-wiki-news-subwords-300',
            'word2vec-google-news-300',
            'glove-wiki-gigaword-300'
        ]

    logbooks = []
    for e in args.embedding:
        for i in range(10):
            emb = gensim.downloader.load(e)
            embedding_mat = load_pretrained_embedding(
                emb,
                vocab=dict(zip(dataset.index.values, range(dataset.shape[0]))),
                embedding_space=emb.vector_size,
            )[:-1]

            # pca = PCA(n_components='mle')
            # pca.fit(embedding_mat)
            # embedding_mat = pca.transform(embedding_mat)
            #
            # logging.info(f"Variance ratios: {np.sum(pca.explained_variance_ratio_)}")
            # logging.info(f"New embeddings: {embedding_mat.shape}")

            log = cross_validate(
                regressor,
                embedding_mat,
                targets,
                verbose=1,
                n_jobs=-1,
                #return_train_score=True,
                cv=KFold(n_splits=args.folds),
                scoring=('r2', 'neg_mean_squared_error', 'neg_mean_absolute_error'),
            )

            log = pd.DataFrame(log)
            log = log.round(2).describe(percentiles=[.05, .15, .25, .5, .75, .85, .95])
            logbooks.append(log)

        df = pd.concat(logbooks).groupby(level=0).mean()
        logging.info(df)
        df.to_csv(f'{outdir}/crossval_{e}.csv')

    logging.info(f"Total time elapsed: {time.time() - timeit:.2f} sec.")


if __name__ == "__main__":
    main()

