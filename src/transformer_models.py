import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
os.environ["TOKENIZERS_PARALLELISM"] = "false"

import sys
import logging
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras import Model
from transformers import TFAutoModel


logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s'
)
logger = logging.getLogger(__name__)


class Default(Model):
    """
    Args:
        max_length: max number of words allowed for input
        codename: pretrained model from huggingface

    Returns:
        Keras Model
    """

    def __init__(self, max_length: int, codename='distilbert-base-uncased'):
        super(Default, self).__init__()
        self.max_token_len = max_length

        self.transformer = TFAutoModel.from_pretrained(
            codename,
            trainable=True
        ).distilbert

        self.conv = layers.Conv1D(128, kernel_size=5, activation='relu')
        self.pool = layers.MaxPooling1D()
        self.bidirectional = layers.Bidirectional(
            layers.LSTM(
                64,
                return_sequences=True,
                dropout=0.1,
                recurrent_dropout=0.1)
        )
        self.dropout = layers.Dropout(0.5)
        self.regressor = layers.Dense(1, activation='linear')

    def model(self):
        inputs = layers.Input(
            shape=(self.max_token_len,),
            dtype=tf.int32,
            name='input_ids'
        )

        return Model(inputs=inputs, outputs=self.call(inputs))


class Baseline(Default):
    def call(self, inputs, training=True, **kwargs):

        emb = self.transformer(inputs).last_hidden_state[:, 0, :]

        x = layers.Dense(128, activation='relu')(emb)
        x = self.dropout(x, training=training)

        x = layers.Dense(64, activation='relu')(x)
        x = self.dropout(x, training=training)

        x = layers.Dense(32, activation='relu')(x)
        return self.regressor(x)


class LSTM(Default):
    def call(self, inputs, training=True, **kwargs):

        emb = self.transformer(inputs).last_hidden_state[:, 0, :]
        x = self.conv(emb)
        x = self.pool(x)
        x = self.bidirectional(x)
        x = layers.Flatten()(x)
        x = layers.Dense(64, activation='relu')(x)
        x = self.dropout(x, training=training)

        x = layers.Dense(32, activation='relu')(x)
        return self.regressor(x)
