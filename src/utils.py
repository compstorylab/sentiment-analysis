import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
os.environ["TOKENIZERS_PARALLELISM"] = "false"

import logging
import sys
import io
import re
import json
import pandas as pd
import numpy as np
from pathlib import Path
from typing import List, Any
from tqdm import tqdm
import requests
import multiprocessing as mp
from tensorflow.keras import Model
from tensorflow.keras.models import load_model
from transformers import AutoTokenizer
from tensorflow.keras.preprocessing.text import Tokenizer, tokenizer_from_json
from tensorflow.keras.preprocessing.sequence import pad_sequences



logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s'
)
logger = logging.getLogger(__name__)


def multiprocess(func, jobs, cores):
    """ Multiprocess a generic function
    Args:
        func: a python function
        jobs: a list of jobs for function `func`
        cores: number of cores to use
        Returns:
            an array of outputs for every function call
    """
    jobs = list(jobs)
    if cores == 1:
        logs = []
        for j in jobs:
            logs.append(func(j))
    elif cores == -1:
        with mp.Pool(mp.cpu_count()) as p:
            logs = list(p.map(func, jobs))
    elif cores > 1:
        with mp.Pool(cores) as p:
            logs = list(p.map(func, jobs))
    else:
        logging.error('Error: jobs must be a positive integer')
        return False
    return logs


def get_def(token, lang='en_US'):
    
    req = f"https://api.dictionaryapi.dev/api/v2/entries/{lang}/{token}"
    definitions = []
    
    try:
        response = requests.get(req)
        logbook = json.loads(response.text)
        
        for m in logbook[0]['meanings']:
            for d in m['definitions']:
                definitions.append(d['definition'])
                
    except Exception:
        return token
    
    return f"{token}: {' '.join(definitions).lower()}"


def load_labmt_scores(path: Path) -> pd.DataFrame:
    """ Load labMT scores into dataframe
    Args:
        path: path to English labmt scores
    """
    df = pd.read_csv(
        path,
        header=0,
        na_filter=False,
        sep='\t',
        usecols=['word', 'happiness', 'standard_deviation_of_ratings', 'def']
    )
    df.columns = df.columns.str.replace(' ', '_').str.lower()
    df.set_index('word', inplace=True)
    return df.sample(frac=1)


def load_ousiometer_scores(path: Path) -> pd.DataFrame:
    """ Load Ousiometer scores into a dataframe
    Args:
        path: path to English Ousiometer scores
    """
    df = pd.read_csv(
        path,
        header=0,
        na_filter=False,
        sep='\t',
        usecols=['word', 'power', 'danger', 'structure', 'def']
    )
    df.index = df.index.astype(str)
    df = df.drop_duplicates(subset='word')
    df.set_index('word', inplace=True)
    df = df.apply(pd.to_numeric, errors='coerce')
    return df.sample(frac=1)


def load_vad_scores(path: Path) -> pd.DataFrame:
    """ Load VAD lex scores into a dataframe
    Args:
        path: path to English VAD scores
    """
    df = pd.read_csv(
        path,
        header=0,
        na_filter=False,
        sep='\t',
        usecols=['word', 'valence', 'arousal', 'dominance', 'def']
    )
    df.index = df.index.astype(str)
    df = df.drop_duplicates(subset='word')
    df.set_index('word', inplace=True)
    df = df.apply(pd.to_numeric, errors='coerce')
    return df.sample(frac=1)


def load_unknown_table(path: Path) -> pd.DataFrame:
    """ Load table into a dataframe
    Args:
        path: path to .tsv/.csv file
    """
    table = pd.read_csv(
        path,
        sep='\t' if path.suffix == '.tsv' else ',',
        index_col='word',
        header=0,
        na_filter=False,
    )
    return table


def sample_balanced_dataset(dataset: pd.DataFrame, target: str, frac: float = .6):
    """
    Create a balanced subsample of the dataset
    Args:
        dataset: dataframe of labMT scores or VAD
        target: name of the target col
        frac: sample fraction size
    Returns:
        balanced dataset
    """
    unique, counts = np.unique(dataset[target].round(2), return_counts=True)
    weights = dict(zip(unique, (np.sum(counts)/counts)))
    dataset['weights'] = [weights.get(k) for k in dataset[target].round(2)]
    return dataset.sample(frac=frac, weights='weights')


def load_pretrained_embedding(emb: Any, vocab: dict, embedding_space: int) -> np.array:
    """ load pretrained embedding
    Args:
        emb: path to fasttext .vec file or gensim model
        vocab: word dict
        embedding_space: size of the embeddings space
    Returns:
        embedding matrix
    """

    if isinstance(emb, Path):
        embedding = {}
        with io.open(emb, 'r', encoding='utf-8', newline='\n', errors='ignore') as file:
            for line in tqdm(file, desc="Loading pretrained embedding"):
                tokens = line.rstrip().rsplit(' ')
                embedding[tokens[0]] = np.asarray(tokens[1:], dtype='float32')
    else:
        embedding = emb

    missing_words = []  # OOV
    embedding_mat = np.zeros((len(vocab)+1, embedding_space))
    for w, i in vocab.items():
        w = re.sub(r'#|-|▁|\s+', '', w)

        try:
            vec = embedding[w]
            if (vec is not None) and len(vec) > 0:
                embedding_mat[i] = vec

        except KeyError:
            missing_words.append(w)

    logger.info(f"Number of missing words embeddings: {np.sum(np.sum(embedding_mat, axis=1) == 0)}")
    logger.info(f"Random sample: {np.random.choice(missing_words, 10)}")
    return embedding_mat


def tokenize(docs: List[str], max_length: int = 50, verbose: int = 0) -> dict:
    """
    Preprocess data for a transformer model
    Args:
        docs: a list of input strings
        max_length: max number of words allowed for input
        verbose: a toggle for progressbar

    Returns:
        Encoded input for a transformer model
        dict(['input_ids'], ['attention_mask'], ['token_type_ids'], ['subwords])
    """

    tokenizer = AutoTokenizer.from_pretrained(
        'distilbert-base-uncased',
        do_lower_case=True,
    )

    ids, masks, segs, subwords = [], [], [], []
    if verbose:
        docs = tqdm(docs, desc='Tokenizing documents')

    for sentence in docs:
        inputs = tokenizer.encode_plus(
            sentence,
            truncation=True,
            padding='max_length',
            max_length=max_length,
            add_special_tokens=True,
            return_attention_mask=True,
            return_token_type_ids=True,
        )
        ids.append(inputs['input_ids'])
        masks.append(inputs['attention_mask'])
        segs.append(inputs['token_type_ids'])
        subwords.append(
            tokenizer.convert_ids_to_tokens(inputs['input_ids'], skip_special_tokens=True)
        )

    return dict(
        input_ids=np.asarray(ids, dtype='int32'),
        attention_mask=np.asarray(masks, dtype='int32'),
        token_type_ids=np.asarray(segs, dtype='int32'),
        subwords=np.asarray(subwords, dtype='object'),
    )


def char_ngram_tokenize(
    docs: List[str],
    tokenizer_path: Path,
    max_length: int = 50,
    verbose: int = 0
) -> dict:
    """
    Load tokenizer from disk, or fit a new one
    Args:
        docs: a list of strings
        tokenizer_path: path vocab .json file
        max_length: max number of subwords allowed to be scored as a token
        verbose: a toggle for progressbar

    Returns:
        encoded tokens
    """
    def breakup_words(x):
        token_emb_3 = [x[i:i + 3] for i in range(len(x) - 3 + 1)]
        token_emb_4 = [x[i:i + 4] for i in range(len(x) - 4 + 1)]
        token_emb_5 = [x[i:i + 5] for i in range(len(x) - 5 + 1)]
        token_emb = set().union([x], token_emb_3, token_emb_4, token_emb_5)
        return list(token_emb)

    if verbose:
        docs = tqdm(docs, desc='Tokenizing documents')

    subwords = [breakup_words(w) for w in docs]

    try:
        with tokenizer_path.open(mode='r') as f:
            t = tokenizer_from_json(json.load(f))

    except FileNotFoundError:
        t = Tokenizer(
            filters='!"\'#$%&()*+,./:;<=>?@[\\]^`{|}~\t\n',  # keeping {-} and {_}
            lower=True,
            char_level=False,
            oov_token='<UNK>'
        )
        t.fit_on_texts(subwords)

        with tokenizer_path.open(mode='w') as savefile:
            json.dump(
                t.to_json(),
                savefile,
                sort_keys=True,
                indent=4,
                default=lambda x: x.item()
            )

    tokens = t.texts_to_sequences(subwords)
    seq = pad_sequences(tokens, maxlen=max_length)

    return dict(
        input_ids=np.asarray(seq, dtype='int'),
        subwords=subwords,
        vocab=t.word_index
    )


def preprocess(
    data: pd.DataFrame,
    embedding: Path,
    subword_parser: Path,
    max_token_len: int,
    embedding_space: int,
) -> (np.array, np.array):
    """ Preprocess data and compute embedding matrix
    Args:
        data: a dataframe of words
        embedding: path to pretrained word embeddings
        subword_parser: path to save new model for parsing subwords
        max_token_len: max number of ngrams allowed to be scored as a token
        embedding_space: size of the embeddings space
    Returns:
        vectorized sequences and embedding matrix
    """

    inputs = char_ngram_tokenize(
        docs=data.index.tolist(),
        tokenizer_path=subword_parser,
        max_length=max_token_len,
        verbose=1
    )

    embedding_mat = load_pretrained_embedding(
        embedding,
        vocab=inputs['vocab'],
        embedding_space=embedding_space
    )
    return inputs['input_ids'], embedding_mat


def bootstrap_predict(
    model: Model,
    inputs: List[int],
    min_val: int,
    max_val: int,
    batch_size: int,
    n_samples: int = 100,
):
    """
    Average predictions and compute stdev
    Args:
        model: a Keras Model
        inputs: encoded tokens to be processed
        min_val: minimum score
        max_val: maximum score
        n_samples: number of predictions of average
        batch_size: number of samples per batch
    Returns:
        average prediction, stdev
    """
    def batch_generator(arr, s):
        for k in range(0, len(arr), s):
            yield arr[k:k + s]

    preds = np.zeros((len(inputs), n_samples))

    for i in tqdm(
        range(n_samples),
        desc='Predictions',
        #unit_scale=len(inputs),
    ):
        b = []
        for batch in batch_generator(inputs, s=batch_size):
            b.extend(model(batch, training=True).numpy().flatten())
        preds[:, i] = b

    preds = np.clip(preds, a_min=min_val, a_max=max_val)
    return np.mean(preds, axis=1), np.std(preds, axis=1)


def ensemble_predict(
    path: Path,
    tokens: List[str],
    min_val: int,
    max_val: int,
    batch_size: int,
    tokenizer_path: Path = None
):
    """ Average predictions from a set of pretrained models
    Args:
        path: path to a directory with pretrained model(s)
        tokens: ngrams to be processed
        min_val: minimum score
        max_val: maximum score
        batch_size: number of samples per batch
        tokenizer_path: path to pretrained subword tokenizer for fasttext only
    Returns:
        subwords, average prediction, stdev
    """

    inputs = None
    means, stds, = [], []

    for i, p in enumerate(path.iterdir()):
        if p.is_dir() and list(p.glob('*.pb')) != []:
            model = load_model(p)

            if inputs is None:
                input_shape = model.layers[0].input_shape[0]

                if 'fasttext' in str(p):
                    inputs = char_ngram_tokenize(
                        docs=tokens,
                        tokenizer_path=tokenizer_path,
                        max_length=input_shape[1],
                    )
                else:
                    inputs = tokenize(
                        docs=tokens,
                        max_length=input_shape[1],
                    )

            pred, stdev = bootstrap_predict(
                model,
                inputs=inputs['input_ids'],
                min_val=min_val,
                max_val=max_val,
                batch_size=batch_size
            )
            means.append(pred)
            stds.append(stdev)
            del model

    means = np.asarray(means)
    stds = np.asarray(stds)
    subwords = [' '.join(t) for t in inputs['subwords']]
    return subwords, np.mean(means, axis=0), np.mean(stds, axis=0)


def eval_model(
    ensemble: Path,
    scores: pd.DataFrame,
    target: str,
    batch_size: int,
    tokenizer: Path = None
):
    """ Evaluate a single model
    Args:
        ensemble: path to a directory with pretrained model(s)
        scores: a dataframe of words and their scores
        target: name of the target column
        batch_size: number of samples per batch
        tokenizer: path to pretrained subword tokenizer for fasttext only
    Returns:
        updated scores along with predictions
    """
    tokens = scores.index.tolist() if 'fasttext' in str(ensemble) else scores['def'].tolist()

    scores['subwords'], scores['prediction'], scores['stdev'] = ensemble_predict(
        ensemble,
        tokens=tokens,
        min_val=scores[target].min(),
        max_val=scores[target].max(),
        batch_size=batch_size,
        tokenizer_path=tokenizer
    )
    scores['mae'] = np.abs(scores['prediction'] - scores[target])
    scores.sort_values('mae', inplace=True)
    print(scores[['subwords', target, 'prediction', 'stdev', 'mae']].round(2).tail(50))
    print('-'*75)
    print(scores['mae'].describe(percentiles=[.25, .5, .75, .85, .95]))
    return scores
