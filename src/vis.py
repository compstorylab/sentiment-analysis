import pandas as pd
import seaborn as sns
import numpy as np
import scipy as sp
from pprint import pprint
from matplotlib.colors import from_levels_and_colors
import matplotlib.pyplot as plt


def plot_conf(scores, dim, savedir):
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 12,
        'axes.labelsize': 12,
        'xtick.labelsize': 10,
        'ytick.labelsize': 10,
        'legend.fontsize': 10,
    })

    scores = scores.sort_values('mae', ascending=False).iloc[:50]

    preds = scores[['prediction', 'mae', 'stdev']]
    preds = preds.rename(columns={'prediction': 'sentiment'})
    preds['label'] = 'Prediction'
    
    try: 
        y = scores[[dim, 'standard_deviation_of_ratings']]
        y = y.rename(columns={dim: 'sentiment', 'standard_deviation_of_ratings': 'stdev'})
    except KeyError:
        y = scores[[dim]]
        y = y.rename(columns={dim: 'sentiment'})
        
    y['label'] = 'Human ratings'

    df = pd.concat([preds, y]).reset_index()

    g = sns.PairGrid(
        df,
        x_vars=['mae', 'sentiment', 'stdev'],
        y_vars=['word'],
    )
    g.fig.set_size_inches(6, 9)
    g.map(
        sns.scatterplot,
        hue=df['label'],
        style=df['label'],
        size=df['label'],
        hue_order=['Human ratings', 'Prediction'],
        palette={'Human ratings': "dimgrey", "Prediction": 'C0'},
        sizes={'Human ratings': 60, "Prediction": 60},
        x_jitter=0,
        y_jitter=0,
        zorder=3,
    )
    g.set(xlabel="", ylabel="")
    g.add_legend(title="", adjust_subtitles=True, loc='upper left')

    titles = ["MAE", r'$h_{avg}$', "STDEV"]
    lims = [
        (-.1, 3),
        (1, 9),
        (-.1, 3),
    ]

    for ax, title, lim in zip(g.axes.flat, titles, lims):
        ax.set(title=title)
        ax.grid(True, which="both", axis='both', lw=1, linestyle='-', zorder=0)
        ax.set_xlim(lim)

    sns.despine(left=True, bottom=True)
    plt.tight_layout()

    plt.savefig(f'{savedir}/{dim}_conf.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savedir}/{dim}_conf.png', dpi=300, bbox_inches='tight', pad_inches=.25)


def plot_mae_heatmap(scores, dim, savedir):
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 14,
        'axes.labelsize': 12,
        'xtick.labelsize': 10,
        'ytick.labelsize': 10,
        'legend.fontsize': 10,
    })

    fig, ax = plt.subplots(figsize=(4, 4))
    scores['mae'] = np.clip(scores['mae'], a_min=0, a_max=8)
    g = sns.JointGrid(
        data=scores,
        x=dim,
        y="mae",
        marginal_ticks=True
    )
    g.ax_joint.set(
        ylabel='Mean absolute error', 
        xlabel=dim.capitalize(),
        xlim=(1, 9),
        ylim=(0, 3)
    )

    cax = g.fig.add_axes([.75, .6, .02, .2])
    colors = sns.color_palette('magma_r', 5)
    levels = np.array([1, 5, 10, 15, 20])
    cmap, norm = from_levels_and_colors(levels, colors, extend="max")

    g.plot_joint(
        sns.histplot,
        pmax=.8,
        cbar=True,
        cbar_ax=cax,
        cmap=cmap,
        norm=norm,
    )
    cax.set_ylabel('Tokens')
    cax.yaxis.set_label_position("left")

    g.plot_marginals(sns.histplot, color='k', bins=100)

    plt.savefig(f'{savedir}/{dim}_heatmap.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savedir}/{dim}_heatmap.png', dpi=300, bbox_inches='tight', pad_inches=.25)


def plot_labmt_mae_dists(scores, savedir):
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 14,
        'axes.labelsize': 12,
        'xtick.labelsize': 10,
        'ytick.labelsize': 10,
        'legend.fontsize': 10,
    })

    fig, axes = plt.subplots(2, 2, figsize=(8, 8))

    axes[0, 0].hist(scores["mae"], bins=100, label=r'$h_{avg} \in [1, 9]$', color='C0')
    axes[0, 0].set_yscale('log')
    axes[0, 0].set_xlim(0, 2)
    axes[0, 0].set_ylim(1, 10 ** 4)
    axes[0, 0].set_ylabel('Number of tokens')
    mae = scores["mae"].mean()
    axes[0, 0].axvline(mae, color='k', ls='--', label=f'MAE={mae:.3f}')
    axes[0, 0].legend(frameon=False)

    ss = scores[scores['happiness'].between(4, 6, inclusive=True)]["mae"]
    axes[0, 1].hist(ss, bins=100, label=r'$h_{avg} \in [4, 6]$', color='grey')
    axes[0, 1].set_yscale('log')
    axes[0, 1].set_xlim(0, 2)
    axes[0, 1].set_ylim(1, 10 ** 4)
    mae = ss.mean()
    axes[0, 1].axvline(mae, color='k', ls='--', label=f'MAE={mae:.3f}')
    axes[0, 1].legend(frameon=False)

    ss = scores[scores['happiness'] < 4]["mae"]
    axes[1, 0].hist(ss, bins=100, label=r'$h_{avg} \in [1, 4)$', color='C1')
    axes[1, 0].set_yscale('log')
    axes[1, 0].set_xlim(0, 2)
    axes[1, 0].set_ylim(1, 10 ** 4)
    axes[1, 0].set_xlabel('Absolute error')
    axes[1, 0].set_ylabel('Number of tokens')
    mae = ss.mean()
    axes[1, 0].axvline(mae, color='k', ls='--', label=f'MAE={mae:.3f}')
    axes[1, 0].legend(frameon=False)

    ss = scores[scores['happiness'] > 6]["mae"]
    axes[1, 1].hist(ss, bins=100, label=r'$h_{avg} \in (6, 9]$', color='C2')
    axes[1, 1].set_yscale('log')
    axes[1, 1].set_xlim(0, 2)
    axes[1, 1].set_ylim(1, 10 ** 4)
    axes[1, 1].set_xlabel('Absolute error')
    mae = ss.mean()
    axes[1, 1].axvline(mae, color='k', ls='--', label=f'MAE={mae:.3f}')
    axes[1, 1].legend(frameon=False)

    plt.savefig(f'{savedir}/labmt_dists.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savedir}/labmt_dists.png', dpi=300, bbox_inches='tight', pad_inches=.25)


def plot_ousiometer_mae_dists(scores, dim, savedir):
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 14,
        'axes.labelsize': 12,
        'xtick.labelsize': 10,
        'ytick.labelsize': 10,
        'legend.fontsize': 10,
    })

    fig, axes = plt.subplots(2, 2, figsize=(8, 8))

    axes[0, 0].hist(scores["mae"], bins=100, label=r'$s \in [-1.0, 1.0]$', color='C0')
    axes[0, 0].set_yscale('log')
    axes[0, 0].set_xlim(0, 1)
    axes[0, 0].set_ylim(1, 5 * 10 ** 4)
    axes[0, 0].set_ylabel('Number of tokens')
    mae = scores["mae"].mean()
    axes[0, 0].axvline(mae, color='k', ls='--', label=f'MAE={mae:.3f}')
    axes[0, 0].legend(frameon=False)

    ss = scores[scores[dim].between(-.3, .3, inclusive=True)]["mae"]
    axes[0, 1].hist(ss, bins=100, label=r'$s \in [-0.3, 0.3]$', color='grey')
    axes[0, 1].set_yscale('log')
    axes[0, 1].set_xlim(0, 1)
    axes[0, 1].set_ylim(1, 5 * 10 ** 4)
    mae = ss.mean()
    axes[0, 1].axvline(mae, color='k', ls='--', label=f'MAE={mae:.3f}')
    axes[0, 1].legend(frameon=False)

    ss = scores[scores[dim] < -.3]["mae"]
    axes[1, 0].hist(ss, bins=100, label=r'$s \in [-1.0, -0.3)$', color='C1')
    axes[1, 0].set_yscale('log')
    axes[1, 0].set_xlim(0, 1)
    axes[1, 0].set_ylim(1, 5 * 10 ** 4)
    axes[1, 0].set_xlabel('Absolute error')
    axes[1, 0].set_ylabel('Number of tokens')
    mae = ss.mean()
    axes[1, 0].axvline(mae, color='k', ls='--', label=f'MAE={mae:.3f}')
    axes[1, 0].legend(frameon=False)

    ss = scores[scores[dim] > .3]["mae"]
    axes[1, 1].hist(ss, bins=100, label=r'$s \in (0.3, 1.0]$', color='C2')
    axes[1, 1].set_yscale('log')
    axes[1, 1].set_xlim(0, 1)
    axes[1, 1].set_ylim(1, 5 * 10 ** 4)
    axes[1, 1].set_xlabel('Absolute error')
    mae = ss.mean()
    axes[1, 1].axvline(mae, color='k', ls='--', label=f'MAE={mae:.3f}')
    axes[1, 1].legend(frameon=False)

    plt.savefig(f'{savedir}/{dim}_dists.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savedir}/{dim}_dists.png', dpi=300, bbox_inches='tight', pad_inches=.25)


def plot_errorbars(scores, dim, savedir):
    scores = scores.rename(columns={'standard_deviation_of_ratings': 'ratings_stdev'})
    iou = {}
    for w, row in scores.iterrows():
        iou[w] = row['mae'] <= (row['stdev'] + row['ratings_stdev'])
    scores['iou'] = pd.Series(iou)
    scores = scores[~scores['iou']]
    print(scores[~scores['iou']])
    
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 12,
        'axes.labelsize': 12,
        'xtick.labelsize': 10,
        'ytick.labelsize': 10,
        'legend.fontsize': 10,
    })

    scores = scores.sort_values('ratings_stdev', ascending=False).iloc[:25].reset_index()

    fig, ax = plt.subplots(figsize=(4, 10))

    ax.errorbar(
        scores[dim],
        scores['word'],
        xerr=scores['ratings_stdev'],
        label='Ratings stdev',
        fmt='x',
        color='dimgrey',
        capsize=5,
        elinewidth=2,
    )

    ax.errorbar(
        scores['prediction'],
        scores['word'],
        xerr=scores['stdev'],
        label='Prediction stdev',
        fmt='o',
        color='C0',
        capsize=5,
        elinewidth=2,
    )

    ax.grid(True, which="both", axis='both', lw=1, linestyle='-', zorder=0)
    ax.set_xlim(1, 9)
    ax.set_xticks(range(1, 10))
    ax.invert_yaxis()
    ax.set_xlabel('Sentiment')
    ax.legend(frameon=False, loc='upper left', bbox_to_anchor=(-.4, 1.04))

    sns.despine(left=True, bottom=True)
    plt.savefig(f'{savedir}/{dim}_errorbars.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savedir}/{dim}_errorbars.png', dpi=300, bbox_inches='tight', pad_inches=.25)


def plot_cross_val(logbook, savepath):
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 14,
        'axes.labelsize': 12,
        'xtick.labelsize': 10,
        'ytick.labelsize': 10,
        'legend.fontsize': 10,
    })

    fig, ax = plt.subplots(figsize=(4, 4))

    means = logbook.groupby('epoch').mean()
    stds = logbook.groupby('epoch').std()

    ax.plot(means['mae'], color='dimgrey', label="Training MAE")
    plt.fill_between(
        stds['mae'].index,
        means['mae'] - stds['mae'],
        means['mae'] + stds['mae'],
        color='dimgrey',
        alpha=.3,
        label=r'Training MAE $\pm$ stdev'
    )

    ax.plot(means['val_mae'], color='C0', label="Validation MAE")
    plt.fill_between(
        stds['mae'].index,
        means['val_mae'] - stds['val_mae'],
        means['val_mae'] + stds['val_mae'],
        color='C0',
        alpha=.3,
        label=r'Validation MAE $\pm$ stdev'
    )

    ax.set_xlim([0, len(means)])
    ax.set_ylim([0, 2])

    ax.set_xlabel('Epochs')
    ax.set_ylabel('Mean absolute error')
    ax.legend(loc="upper right")

    ax.grid(which='both')
    plt.savefig(f'{savepath}.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savepath}.png', dpi=300, bbox_inches='tight', pad_inches=.25)


def plot_conf_effectiveness(scores, dim, minn, maxx, step, savedir):
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 14,
        'axes.labelsize': 12,
        'xtick.labelsize': 10,
        'ytick.labelsize': 10,
        'legend.fontsize': 10,
    })
    x = scores['stdev']
    y = scores['mae']
    intvs = pd.cut(scores[dim], np.arange(minn, maxx + step, step=step), labels=False)
    bounds = sorted(pd.cut(scores[dim], np.arange(minn, maxx + step, step=step)).unique())

    g = sns.jointplot(
        x=x,
        y=y,
        s=20,
        marker=".",
        hue=intvs,
        legend='full',
        palette='viridis',
        edgecolor='none',
        xlim=(0.05, .8),
        ylim=(-.05, 2),
        alpha=.66
    )

    sns.regplot(
        x=x,
        y=y,
        ax=g.ax_joint,
        line_kws=dict(color='orangered'),
        scatter_kws=dict(alpha=0.0)
    )

    g.fig.set_size_inches(6, 6)

    handles, labels = g.ax_joint.get_legend_handles_labels()
    g.ax_joint.legend(
        handles=handles,
        labels=[fr"$S \in {b}$" for i, b in zip(range(1, len(bounds) + 1), bounds)],
        title=f"{dim.capitalize()}",
        loc='upper right'
    )

    slope, intercept, r_value, p_value, std_err = sp.stats.linregress(x, y)
    g.ax_joint.text(.1, .95, f'r={r_value:.2f}, p={p_value}', transform=g.ax_joint.transAxes)

    g.set_axis_labels(ylabel="Mean absolute error", xlabel="Prediction stdev")

    plt.savefig(f'{savedir}/{dim}_effectiveness.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savedir}/{dim}_effectiveness.png', dpi=300, bbox_inches='tight', pad_inches=.25)


def plot_happiness(df, savedir):
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 14,
        'axes.labelsize': 12,
        'xtick.labelsize': 10,
        'ytick.labelsize': 10,
        'legend.fontsize': 10,
    })

    g = sns.JointGrid(
        data=df,
        x="happiness",
        y="standard_deviation_of_ratings",
        marginal_ticks=True
    )

    cax = g.fig.add_axes([.75, .6, .02, .2])
    colors = sns.color_palette('magma_r', 5)
    levels = np.array([1, 5, 10, 15, 20])
    cmap, norm = from_levels_and_colors(levels, colors, extend="max")

    g.plot_joint(
        sns.histplot,
        pmax=.8,
        cbar=True,
        cbar_ax=cax,
        cmap=cmap,
        norm=norm,
    )

    g.ax_joint.set(
        ylabel='Ratings STDEV',
        xlabel='Happiness',
        xlim=(1, 9),
        ylim=(0, 3)
    )

    cax.set_ylabel('Tokens')
    cax.yaxis.set_label_position("left")

    g.plot_marginals(sns.histplot, color='k', bins=100)
    plt.savefig(f'{savedir}/happiness.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savedir}/happiness.png', dpi=300, bbox_inches='tight', pad_inches=.25)
